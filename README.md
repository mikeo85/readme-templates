# Project Title

**One-line summary of project.** \
*[Or just put some ascii art here](http://www.patorjk.com/software/taag)*

## Description

Paragraph. *(Omit heading and paragraph if one-line summary above is enough.)*

*Repo URL: [URL](URL)*

## Features

The main features include:

* TBD

## Prerequisites / Dependencies

* As needed

## Installation and Setup

* Clone the repo into a working folder.
* Any other preparatory steps

## Execution

* How to run the program
* Step-by-step bullets
```
code blocks for commands
```

## Post-Execution

* As needed

## Additional Notes

* TBD

## Todo

* TBD

## Metadata

**Contributors**

* Mike Owens | [GitLab](https://gitlab.com/mikeo85) ~ [GitHub](https://github.com/mikeo85) ~ [Twitter](https://twitter.com/quietmike8192)

**License: [{TYPE: MIT, GPL, etc.}](LICENSE)** * * * * *Note: [Here's a quick summary of various open source licenses](https://www.toptal.com/open-source/developers-guide-to-open-source-licenses).*

**Version History**

* See [commits](../../commits) or [release history](../../releases).

## Acknowledgments

*Inspiration, code snippets, etc.*

* [DomPizzie/README-Template.md](https://gist.github.com/DomPizzie/7a5ff55ffa9081f2de27c315f5018afc)
* [dbader/readme-template](https://github.com/dbader/readme-template)
* [ASCII art](http://www.patorjk.com/software/taag)
* [TBD](URL)
